var Migrations = artifacts.require("./Migrations.sol");
var M87Token = artifacts.require("./M87Token.sol");
var Ownable = artifacts.require("./Ownable.sol");
var SafeMath = artifacts.require("./SafeMath.sol");

module.exports = async(deployer)=> {

  deployer.deploy(Migrations);
  deployer.deploy(SafeMath);
  deployer.deploy(Ownable);
  deployer.deploy(M87Token);
};
