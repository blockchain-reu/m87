const M87Token = artifacts.require("./M87Token.sol");
var fs = require('fs');
var csv = require('fast-csv');


contract('Basic M87 Token', async (accounts) =>{

  beforeEach('setup contract for each test', async function () {
    await M87Token.new({from:accounts[0]});
  })

  it('Run stats tests with varying params', async() => {
    const instance = await M87Token.deployed();
    
    // PARAMETERS
    
    // Due to constraints of the Ethereum blockchain, you cannot send a single
    // Ethereum transaction that will send tokens to every single user at once.
    // Thus, you must send multiple transactions and each transaction sends
    // tokens in batches to a certain number of users at a time. The optimal 
    // batch size would be the largest possible batch because there is a fixed 
    // cost to sending a transaction, thus a larger batch size reduces the 
    // number of transactions.
    // The number 390 is optimized to make sure the airdrop would not completely
    // prevent other people from transacting on the Ethereum blockchain while
    // the airdrop is occurring, while also being a relatively large number.
    // In other words, M87 would send 390 users tokens in each transaction.
    const batchSize = 390

    // To send an Ethereum transaction, the transaction's cost is denominated
    // in units called "gas." This paramater 'gasProvided' is the max amount of gas
    // M87 would be willing to use to send each individual transaction.
    // This number cannot exceed the Ethereum block gas limit. This limit is
    // the maximum amount of gas the Ethereum network can handle every 15
    // seconds (i.e. in each block in its blockchain). The current gas limit 
    // on the main net is ~8m.
    // We assume M87 will want to fill up blocks 3/4 of the way to allow 
    // 1/4 of each block to remain available to the rest of the Ethereum 
    // network. If M87 were to use the maximum (~8m), then no one would be able to 
    // transact on the Ethereum network, or the price to do so would be
    // driven up to very high levels. If M87 were to use too low of a number,
    // (or too low of a batchSize), then M87 would need to execute many
    // Ethereum transactions to complete the airdrop, which would result
    // in a higher cost.
    const gasProvided = 6000000

    // This is the amount of tokens each user will receive in each airdrop. 
    // Changing this number has no impact on the cost to M87. 
    // We assumed 2 for simplicity.
    const tokens_to_each_address = 2

    // The more users, the more transactions required. The fewer users,
    // the fewer transactions required.
    // low 100,000 base case of 1,000,000, worst case 100,000,000
    let numTotalUsers = 1000

    // The price for each unit of gas in gwei. This is a market rate. M87 can choose
    // the number it wants for this, however, if M87 chooses too low of a number,
    // then its transactions will not get picked up by Ethereum miners to include
    // in the Ethereum blockchain. If M87 chooses too high of a number, this
    // guarantees its transactions get included but increases the cost
    // to actually do the airdrop. 50 is a conservative expected rate to
    // complete the airdrop.
    // low of 10, base case of 50, worst case 100
    let gasPrice = 50

    // The market price of 1 Ether. The higher this is, the higher the cost
    // to M87 to do the airdrop and vise versa.
    let priceOfEthInUSD = 450

    // Test accounts to send tokens to. This test uses 9 distint accounts total.
    // using 9 distinct truffle accounts
    let truffleAccounts = accounts.slice(1, 10)

    // creating an array of size batchSize of only the truffleAccounts
    let batchToDistribute = getBatchArray(truffleAccounts, batchSize)

    // an array of each batch that will be airdropping to
    let allBatches = getAllBatches(batchToDistribute, numTotalUsers, batchSize)

    // Array signifying how many tokens to give to each account
    let pointsToDistribute = new Array(batchSize).fill(tokens_to_each_address)

    // Do the airdrop and get resulting stats
    let airdropStats = await airdrop(
      instance, 
      allBatches, 
      pointsToDistribute,
      accounts[0],
      gasProvided
    )

    // MAKE SURE BALANCE IS AS EXPECTED
    let firstAccountBalance = (await instance.balanceOf.call(accounts[1])).toNumber();

    let numTimesFirstAccountAppearsPerBatch = Math.floor(batchSize / truffleAccounts.length)
    if (batchSize % truffleAccounts.length > 0) {
      numTimesFirstAccountAppearsPerBatch += 1
    }

    let numTimesFirstAccountAppearsInLastBatch = Math.floor(allBatches[allBatches.length - 1].length / truffleAccounts.length)
    if (allBatches[allBatches.length - 1].length % truffleAccounts.length > 0) {
      numTimesFirstAccountAppearsInLastBatch += 1
    }

    const totalTimesFirstAccountAppears = numTimesFirstAccountAppearsPerBatch * (allBatches.length - 1) + numTimesFirstAccountAppearsInLastBatch
    const expectedBalanceForFirstAccount = totalTimesFirstAccountAppears * tokens_to_each_address

    assert.equal(firstAccountBalance, expectedBalanceForFirstAccount);


    // ANALYSIS

    const totalGasUsed = airdropStats[0]
    const gasUsedPerBatch = airdropStats[1]

    const totalGasSpent = totalGasUsed * gasPrice
    const costInWei = web3.toWei(totalGasSpent, 'gwei')
    const costInEth = web3.fromWei(costInWei, 'ether')
    const estimatedGasCostFor1m = gasUsedPerBatch * 1000000 / batchSize * gasPrice
    const costFor1MUsersInWei = web3.toWei(estimatedGasCostFor1m, 'gwei')
    const costFor1MUsersInEth = web3.fromWei(costFor1MUsersInWei, 'ether')

    console.log('Estimated gas cost for 1,000,000 users: ', estimatedGasCostFor1m)
    console.log('Estimated cost in eth for 1m users: ', costFor1MUsersInEth)
    console.log('Estimated cost in USD for 1m users: ', costFor1MUsersInEth * priceOfEthInUSD)

    // Will create 3 CSV files from results. Each csv will show the changing total gas cost
    // to M87 in USD when changing the number of users, gas price, and price of Ether.

    // Each CSV file will have this many data points
    const NUM_DATA_POINTS = 100

    const lowNumUsers = 20000
    const expectedNumUsers = 1000000
    const highNumUsers = 10000000
    const usersArray = getDataPoints(lowNumUsers, highNumUsers, NUM_DATA_POINTS)

    const lowGasPrice = 10
    const expectedGasPrice = gasPrice
    const highGasPrice = 100
    const gasPriceArray = getDataPoints(lowGasPrice, highGasPrice, NUM_DATA_POINTS)

    const lowEthPrice = 300
    const expectedEthPrice = priceOfEthInUSD
    const highEthPrice = 1200
    const ethPriceArray = getDataPoints(lowEthPrice, highEthPrice, NUM_DATA_POINTS)


    createCSVFromDatapoints('users_analysis.csv', usersArray, "Number of Users")
    createCSVFromDatapoints('gas_price_analysis.csv', gasPriceArray, "Gas Price")
    createCSVFromDatapoints('eth_price_analysis.csv', ethPriceArray, "Price of Ether")


    // Gets batch of batchSize accounts to airdrop
    function getBatchArray (accountsForBatch, batchSize) {
      let batchToDistribute = new Array(batchSize)
      const numAccounts = accountsForBatch.length
      for (let i = 0; i < batchSize; i++) {
        batchToDistribute[i] = accountsForBatch[i % numAccounts]
      }
      return batchToDistribute
    }

    // makes array of batch arrays. total number of elements in this array
    // of arrays will be the total number of users
    function getAllBatches (batchArray, totalUsers, batchSize) {
      let allBatches = new Array(Math.floor(totalUsers / batchSize)).fill(batchArray)
      allBatches.push(batchArray.slice(0, totalUsers % batchSize))
      return allBatches
    }


    // Perform the airdrop
    async function airdrop(instance, allBatches, pointsToDistribute, account, gasProvided) {
      let totalGasUsed = 0
      let gasUsedPerBatch = 0
      // Airdrop to all users
      for (let i = 0; i < allBatches.length; i++) {
        let r = await instance.airdropTokens(
          allBatches[i],
          pointsToDistribute,
          {
            from: account,
            gas: gasProvided
          }
        );
  
        // Believe this is gwei
        totalGasUsed += r.receipt.gasUsed
  
        // After first one there is a stretch of batches with same gas cost
        if (i == 1) {
          gasUsedPerBatch = r.receipt.gasUsed
        }
  
      }

      return [totalGasUsed, gasUsedPerBatch]
    }


    // makes array of data points to put in graph
    function getDataPoints(lowVal, highVal, numDataPoints) {
      let dataPoints = []
      let i = lowVal

      const increment = (highVal - lowVal) / numDataPoints

      while (i < highVal) {
        dataPoints.push(i)
        i += increment
      }
      return dataPoints
    }


    // Get the expected total gas cost in usd given parameters
    function getTotalGasCostInUSD(
      gasUsedPerBatch,
      batchSize,
      numUsers,
      gasPrice,
      priceOfEthInUSD
    ) {
      const estimatedGasCost = gasUsedPerBatch * numUsers / batchSize * gasPrice
      const costInWei = web3.toWei(estimatedGasCost, 'gwei')
      const costInEth = web3.fromWei(costInWei, 'ether')
      return costInEth * priceOfEthInUSD
    }


    // Makes csv files in ../analysis folder for further analysis
    function createCSVFromDatapoints(filename, datapoints, xAxisTitle) {
      var csvStream = csv.format({headers: true})

      const analysisDir = __dirname.substring(0, __dirname.length - 4)
      writableStream = fs.createWriteStream(analysisDir + "analysis/" + filename);

      writableStream.on("finish", function(){
        console.log("Csv file '../analysis/" + filename + "' created");
      });

      csvStream.pipe(writableStream);

      for (let i = 0; i < datapoints.length; i++) {
        let newObj = {}

        newObj[xAxisTitle] = datapoints[i]

        if (xAxisTitle === "Number of Users") {
          newObj["Gas Cost in USD"] = getTotalGasCostInUSD(
            gasUsedPerBatch,
            batchSize,
            datapoints[i],
            expectedGasPrice,
            expectedEthPrice
          )
        } else if (xAxisTitle === "Gas Price") {
          newObj["Gas Cost in USD"] = getTotalGasCostInUSD(
            gasUsedPerBatch,
            batchSize,
            expectedNumUsers,
            datapoints[i],
            expectedEthPrice
          )
        } else if (xAxisTitle === "Price of Ether") {
          newObj["Gas Cost in USD"] = getTotalGasCostInUSD(
            gasUsedPerBatch,
            batchSize,
            expectedNumUsers,
            expectedGasPrice,
            datapoints[i]
          )
        }

        csvStream.write(newObj)
      }

      csvStream.end();
    }

  })


});
