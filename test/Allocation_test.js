const M87Token = artifacts.require("./M87Token.sol");
var fs = require('fs');
var csv = require('fast-csv');


//csv parameters
let BATCH_SIZE = 80; //addresses are  read from a csv in batches
let distribData = new Array();
let allocData = new Array();
let distribToken = new Array();
let allocToken= new Array();


//testing parameters
const TOKEN_NAME = "M87";
const TOKEN_SYMBOL = "M87";
const TOTAL_SUPPLY = 10000000;




contract('Basic M87 Token', async (accounts) =>{

  beforeEach('setup contract for each test', async function () {
    await M87Token.new({from:accounts[0]});
  })
  it('has an owner', async function () {
    const instance = await M87Token.deployed();
    assert.equal(await instance.owner(),accounts[0])
  })

  it('Checks if tokens are created', async() => {
    const instance = await M87Token.deployed();
    let tokenName = await instance.name({from:accounts[0]});
    assert.equal(tokenName,TOKEN_NAME);
  })

  it('Checks if token symbol is created', async() => {
    const instance = await M87Token.deployed();
    let tokenSymbol = await instance.symbol({from:accounts[0]});
    assert.equal(tokenSymbol,TOKEN_SYMBOL);
  })

  it('Checks if token total supply is correct', async() => {
    const instance = await M87Token.deployed();
    let tokenTotalSupply = (await instance.totalSupply({from:accounts[0]})).toNumber();
    assert.equal(tokenTotalSupply,TOTAL_SUPPLY);
  })

  //Reads from a csv that has 9 accounts(The first account is the originator of the tokens)
  //Then calls the airdrop tokens function and tests that all the tokens were airdroppd correctly
  it('Checks if tokens are airdropped from csv file', async() => {
    function readFile() {
      var stream = fs.createReadStream("test/data/airdrop.csv");
      let index = 0;
      let batch = 0;

      console.log(`
          --------------------------------------------
          --------- Parsing distrib.csv file ---------
          --------------------------------------------
      ******** Removing beneficiaries without address data
      `);
      var csvStream = csv()
      .on("data", function(data){
        if(data[0]!=null && data[0]!='' ){
          allocData.push(data[0]);
          allocToken.push(data[1]);
          index++;
          if(index >= BATCH_SIZE)
          {
            distribData.push(allocData);
            distribToken.push(allocToken.map(Number));
            allocData = [];
            allocToken = [];
            index = 0;
          }
        }
      })
        .on("end", function(){
           //Add last remainder batch
           distribData.push(allocData);
           distribToken.push(allocToken.map(Number));
           allocData = [];
           allocToken = [];
           setAllocation();

        });
        stream.pipe(csvStream);
    }
    if(accounts[0]){
      console.log("Processing airdrop. Batch size is",BATCH_SIZE, "accounts per transaction");
      readFile();
    }else{
      console.log("Please run the script by providing the address of the contract");
    }
    async function setAllocation() {
      console.log(`
        --------------------------------------------
        ---------Performing allocations ------------
        --------------------------------------------
      `);

      const instance= await M87Token.deployed();
      for(var i = 0;i< distribData.length;i++) {
        let gPrice = 50000000000;
        const instance= await M87Token.deployed();
        console.log("Attempting to allocate M87s to accounts:", distribData[i],"\n\n");
        let r = await instance.airdropTokens(distribData[i],distribToken[i],{from:accounts[0], gas:4500000, gasPrice:gPrice});

        console.log("---------- ---------- ---------- ----------");
        console.log("Allocation + transfer was successful.", r.receipt.gasUsed, "gas used. Spent:",r.receipt.gasUsed * gPrice,"wei");
        console.log("---------- ---------- ---------- ----------\n\n");
        for(var j = 0;j< distribData[i].length;j++) {
          let balance= (await instance.balanceOf.call(distribData[i][j])).toNumber();
          assert.equal(balance,distribToken[i][j]);
        }

      }


    }
  });

});
