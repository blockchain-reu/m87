# M87 token creation and distribution

The M87 token is an ERC20 token that is intended to be distributed as a reward for relaying data in the M87 network. We assume M87 will use a CSV file to keep track of how much data each relayer sends. Each relayer is assumed to have an Ethereum address the relayer can use to receive tokens. M87 can then use the CSV file to send tokens to M87 relayers as often as M87 likes for a cost. The cost ranges on a set of parameters such as the price of Ether and the number of users M87 sends tokens to. Detailed analysis on these parameters is done in the test folder.


## Getting Started

This project was developed and tested locally with [Truffle](https://github.com/ConsenSys/truffle)
and [Ganache](https://github.com/trufflesuite/ganache)

### Dependencies
    - node.js
    - fast-csv.js
    - Truffle
    - Ganache



## Testing the smart contract airdrop

Make sure all dependencies are installed. Open 2 command line windows. In the first window, run the following command:

```
ganache-cli -m 'keep accounts constant'
```

This will start a local Ethereum blockchain running on port 8545. The smart contracts written will interact with this locally hosted blockchain.

In the second command line window, run the following command:

```
truffle test
```

All tests should pass.






## Files and Directories


## `/analysis`

Directory containing csv files and Excel file for analysis.

### `/analysis/eth_price_analysis.csv`

Shows the expected total cost in USD for M87 to do a single airdrop based on the changing price of Ether.

### `/analysis/Excel Analysis.xlsx`

Highlights key assumption inputs to determine total cost in USD to do a single airdrop, provides the formula to calculate total cost in USD, and provides multiple tables showing changing total cost under different scenarios.

### `/analysis/gas_price_analysis.csv`

Shows the expected total cost in USD for M87 to do a single airdrop based on changing gas price.

### `/analysis/users_analysis.csv`

Shows the expected total cost in USD for M87 to do a single airdrop based on different numbers of expected M87 relayers.



## `/build`

Directory built automatically by truffle when truffle compiles all contracts in the `/contracts` directory. Contains all application binary interfaces (ABI's) of compiled contract's.

### `/build/contracts/ERC20.json`

ABI of `ERC20.sol` contract.

### `/build/contracts/ERC20Basic.json`

ABI of `ERC20Basic.sol` contract.

### `/build/contracts/ERC20.json`

ABI of `Migrations.sol` contract.



## `/contracts`

Directory containing pre-compiled contracts written in Solidity.

### `/contracts/M87Token.sol`

Smart contract for the M87 token. Users can transfer tokens from one account to another, check balances of accounts, check the total supply of tokens, and an admin set on the contract can send an arbitrary number of tokens to as many users as the admin wants (i.e. airdrop tokens). The code is inspired by [this airdrop](https://hackernoon.com/how-to-script-an-automatic-token-airdrop-for-40k-subscribers-e40c8b1a02c6). As it is currently constrcuted, when the contract is created, M87 receives the entire initial supply of 10,000,000 tokens. M87 can then send tokens to many users at a time by calling the airdropTokens function.

### `/contracts/IERC20.sol`

Contains the interface for an ERC 20 token copied from [the ERC 20 proposal](https://github.com/ethereum/EIPs/issues/20).

### `/contracts/Migrations.sol`

This contract is [required by truffle](http://truffleframework.com/docs/getting_started/migrations#initial-migration) in order to run tests and deploy other contracts. This feature minimizes cost in re-deploying/upgrading contracts. More can be read about this [here](https://medium.com/@blockchain101/demystifying-truffle-migrate-21afbcdf3264).

### `/contracts/Ownable.sol`

This contract is used in `./M87Token.sol` to provide basic authorization control. It was copied from [here](https://github.com/PolymathNetwork/polymath-token/tree/master/contracts).


### `/contracts/SafeMath.sol`

This contract is used in `./M87Token.sol` to provide safety checks on mathematical operations. Most importantly, it protects against overflows. It is also copied directly from [here](https://github.com/PolymathNetwork/polymath-token/tree/master/contracts).


## `/migrations`

Directory containing the deployment instructions for deploying contracts to the Ethereum blockchain.

### `/migrations/1_initial_migration.js`

This file instructs truffle to deploy each contract.


## `/test`

Directory containing test scripts that run via the command `truffle test`.


### `/test/Allocations_test.js`

Deploys the M87Token.sol contract using the first ganache address and tests that the contract has the correct token name, symbol, and total supply. The other ganache addresses are imported from `airdrops.csv` along with a corresponding number of tokens to be distributed to the addresses.


### `/test/M87Token_test.js`

Runs various tests changing key assumptions to determine the cost to M87 to do a single airdrop. Outputs some analysis performed to the csv files in `/analysis`. The key assumptions are explained in detail in comments inside the file.



### `/test/data`

Directory containing data used in test.


#### `/test/data/airdrop.csv`

Contains the 9 other addresses generated by ganache and arbitrary number of tokens to be distributer to each address. These would be calculated with a Gigabyte to token conversion set by M87.




## `truffle.js`

Configuration file for truffle. It is currently set to listen for an Ethereum blockchain running locally on port 7545 (the default port for [ganache-cli](https://github.com/trufflesuite/ganache-cli)) for ease of testing.




## Authors

* **Justin**
* **Chime**
* **Thomas**




## DISCLAIMER

This code should be audited by Solidity developers with extensive debugging experience before deploying to the main net and using in production. Here are a couple known and respected auditors in the space:

    - [Zeppelin](https://zeppelin.solutions/security-audits/)
    - [ConsenSys Diligence](https://consensys.net/diligence/)
