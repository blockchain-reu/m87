pragma solidity ^0.4.23;

import './IERC20.sol';
import './SafeMath.sol';
import './Ownable.sol';


contract M87Token is IERC20, Ownable{
  using SafeMath for uint256;


  M87Token public M87;


  // M87 Token parameters
  string public name = 'M87';
  string public symbol = 'M87';
  uint256 public totalSupply;

  // M87 Distribution parameters
  uint256 public constant INITIAL_SUPPLY = 10000000;


  mapping (address => uint256) balances;
  mapping (address => mapping (address => uint256)) internal allowed;

  // List of admins allowed to perform airdrop
  mapping (address => bool) public airdropAdmins;

  modifier onlyOwnerOrAdmin() {
    require(msg.sender == owner || airdropAdmins[msg.sender]);
    _;
  }

	function balanceOf(address _owner) public view returns (uint256 balance) {
	  return balances[_owner];
	}

	function transfer(address _to, uint256 _value) public returns (bool success){
    balances[msg.sender] = balances[msg.sender].sub(_value);
    balances[_to] = balances[_to].add(_value);
    emit Transfer(msg.sender, _to, _value);
    return true;
}


	function transferFrom(address _from, address _to, uint256 _value) public returns (bool success){
    require(_to != address(0));
    require(_value <= balances[_from]);
    require(_value <= allowed[_from][msg.sender]);

    balances[_from] = balances[_from].sub(_value);
    balances[_to] = balances[_to].add(_value);
    allowed[_from][msg.sender] = allowed[_from][msg.sender].sub(_value);
    emit Transfer(_from, _to, _value);
    return true;
	}

	function approve(address _spender, uint256 _value) public returns (bool success) {
    allowed[msg.sender][_spender] = _value;
    emit Approval(msg.sender, _spender, _value);
    return true;
	}

	function allowance(address _owner, address _spender) public view returns (uint256 remaining) {
    return allowed[_owner][_spender];
	}

  /**
    * @dev perform a transfer of allocations
    * @param _recipient is a list of recipients
    * @param points is a list of points each recipient will receive
    */
  function airdropTokens(address[] _recipient, uint256[] points) public onlyOwnerOrAdmin {
    uint airdropped;
    for(uint256 i = 0; i< _recipient.length; i++)
    {
      require(transfer(_recipient[i], points[i]));
      airdropped = airdropped.add(points[i]);
    }
  }



	constructor () public {
	  totalSupply = INITIAL_SUPPLY;
	  balances[msg.sender] = INITIAL_SUPPLY;  // Give all of the initial tokens to the contract deployer.

    // Can set an initial allocation to particular addresses here.
    // Make sure to reflect this change in totalSupply as well.
    // As an example, uncomment the last 2 lines to have
    // address 0x13E47Be36452d47ABb9634f84f665353AFF82d0D start
    // with 100 tokens:

    // balances[0x13E47Be36452d47ABb9634f84f665353AFF82d0D] = 100;
    // totalSupply += 100;
  }
}
